import React, { useState, Suspense } from 'react';

import { NavStyles, NavItemStyles } from './components/styles/NavStyles';
import ArTestObject from './components/arTestObject';

import ArjsScene from './components/arjs/ArjsScene';
const MindarScene = React.lazy(() => import('./components/mindar/MindarScene'));

// some dummy data that could be pulled from an API
const data = {
  corsProxy: 'https://immense-refuge-77873.herokuapp.com/',
  arjsPatternURL: 'https://home.jonnypage.ca/iversoft/nft/flowers/flowers',
  mindarPatternURL:
    'https://home.jonnypage.ca/iversoft/mindar-targets/flowers.mind',
  box: {
    color: 'yellow',
    position: { x: 0, y: 0, z: 0 },
    scale: { x: 40, y: 40, z: 40 },
  },
  cone: {
    color: 'green',
    position: { x: 0, y: 1, z: 0 },
    scale: { x: 0.5, y: 1, z: 0.5 },
  },
};

function App() {
  const [library, setLibrary] = useState(0);

  return (
    <div className="app">
      <div className="arjs-loader"></div>
      {library === 'arjs' && (
        <ArjsScene pattern={`${data.corsProxy}${data.arjsPatternURL}`}>
          <ArTestObject data={data} />
        </ArjsScene>
      )}

      {library === 'mindar' && (
        <Suspense fallback={<div>Loading...</div>}>
          <MindarScene pattern={`${data.corsProxy}${data.mindarPatternURL}`}>
            <ArTestObject data={data} />
          </MindarScene>
        </Suspense>
      )}

      <NavStyles>
        <NavItemStyles onClick={() => setLibrary('arjs')}>ARJS</NavItemStyles>
        <NavItemStyles onClick={() => setLibrary('mindar')}>
          MindAR
        </NavItemStyles>
      </NavStyles>
    </div>
  );
}

export default App;
