import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import 'mind-ar';

const MindARStyles = styled.div`
  width: 100vw;
  height: 100vh;
`;

export default function MindarScene({ pattern, children }) {
  const sceneRef = useRef(null);
  const targetRef = useRef(null);

  // mindAR needs this effect to start in React
  useEffect(() => {
    const sceneEl = sceneRef.current;
    const targetEl = targetRef.current;
    const arSystem = sceneEl.systems['mindar-system'];
    sceneEl.addEventListener('renderstart', () => {
      arSystem.start(); // start mindAR
    });
    // detect target found
    targetEl.addEventListener('targetFound', (event) => {
      console.log('target found');
    });

    // detect target lost
    targetEl.addEventListener('targetLost', (event) => {
      console.log('target lost');
    });
    return () => {
      arSystem.stop();
    };
  }, []);
  return (
    <MindARStyles>
      <a-scene
        ref={sceneRef}
        mindar={`
        imageTargetSrc: ${pattern}; 
        autoStart: false; 
        uiLoading: no; 
        uiError: no; 
        uiScanning: no;`}
        color-space="sRGB"
        embedded
        renderer="colorManagement: true, physicallyCorrectLights"
        vr-mode-ui="enabled: false"
        device-orientation-permission-ui="enabled: false"
      >
        <a-camera position="0 0 0" look-controls="enabled: false"></a-camera>
        <a-entity mindar-image-target="targetIndex: 0" ref={targetRef}>
          {children}
        </a-entity>
      </a-scene>
    </MindARStyles>
  );
}
