export default function AEntity({
  geometry = 'primitive: box',
  color = 'red',
  scale = { x: 1, y: 1, z: 1 },
  position = { x: 0, y: 0, z: 0 },
}) {
  const material = `color: ${color}`;
  return (
    <a-entity
      geometry={geometry}
      material={material}
      scale={`${scale.x}, ${scale.y}, ${scale.z}`}
      position={`${position.x}, ${position.y}, ${position.z}`}
    ></a-entity>
  );
}
