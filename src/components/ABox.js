export default function ABox({
  color = 'red',
  scale = { x: 1, y: 1, z: 1 },
  position = { x: 0, y: 0, z: 0 },
}) {
  return (
    <a-box
      color={color}
      scale={`${scale.x}, ${scale.y}, ${scale.z}`}
      position={`${position.x}, ${position.y}, ${position.z}`}
    ></a-box>
  );
}
