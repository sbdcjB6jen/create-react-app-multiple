import React from 'react';
import ReactDOM from 'react-dom';

// Renders an arjs scene directly onto the body dom element.
// arjs gets grumpy if it's in a child element

let filesLoaded = 0;

const aframe = document.createElement('script');
const arjs = document.createElement('script');
const scene = document.createElement('a-scene');

export default class ArjsDomLoader extends React.Component {
  constructor(props) {
    super(props);

    aframe.type = 'text/javascript';
    aframe.src = 'js/aframe.0.8.2.min.js';
    aframe.onload = handleLoad;

    arjs.type = 'text/javascript';
    arjs.src = 'js/aframe-ar-nft.js';
    arjs.onload = handleLoad;

    scene.setAttribute('vr-mode-ui', 'enabled: false;');
    scene.setAttribute(
      'renderer',
      'logarithmicDepthBuffer: true; sortObjects: true;'
    );
    scene.setAttribute('embedded', 'true');
    scene.setAttribute(
      'arjs',
      'trackingMethod: best; sourceType: webcam; sourceWidth: 1280; sourceHeight: 960; displayWidth: 1280; displayHeight: 960;'
    );
    function handleLoad(evt) {
      filesLoaded++;
      if (filesLoaded === 2) {
        // if aframe and arjs are loaded - could probably be a little more elegant
        document.body.prepend(scene);
      }
    }
  }

  componentDidMount() {
    document.head.prepend(aframe);
    document.head.prepend(arjs);

    // document.body.prepend(scene);
  }

  componentWillUnmount() {
    document.body.removeChild(scene);
    document.head.removeChild(aframe);
    document.head.removeChild(arjs);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, scene);
  }
}
