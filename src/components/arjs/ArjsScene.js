import ArjsDomLoader from './ArjsDomLoader';

export default function ArjsScene({ pattern, children }) {
  return (
    <ArjsDomLoader>
      <a-nft
        type="nft"
        url={pattern}
        smooth="true"
        smoothCount="5"
        smoothTolerance="0.01"
        smoothThreshold="1"
      >
        {children}
      </a-nft>
      <a-entity camera></a-entity>
    </ArjsDomLoader>
  );
}
