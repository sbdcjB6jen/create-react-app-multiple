import React from 'react';
import ABox from './ABox';
import AEntity from './AEntity';

export default function ArTestObject({ data = {} }) {
  return (
    <React.Fragment>
      <ABox color={data.box.color} scale={data.box.scale}></ABox>
      <AEntity
        geometry="primitive: cone"
        color={data.cone.color}
        position={data.cone.position}
        scale={data.cone.scale}
      ></AEntity>
    </React.Fragment>
  );
}
